import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ConfluenceComponent} from './confluence.component';

const routes: Routes = [
    {
        path: '',
        component: ConfluenceComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfluenceRoutingModule {
}

export const routedComponents = [
    ConfluenceComponent
];