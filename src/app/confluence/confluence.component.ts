import { Component, OnInit } from '@angular/core';
import { ConfluenceService } from './services/ConfluenceService';
import { XLSXService } from './services/XLSXService';
import {Requests} from '../core/modules/requests/Requests';

import {ExportDialog} from './export-dialog/ExportDialog';
import {MatDialog} from '@angular/material';

@Component({
    selector: 'shop',
    templateUrl: 'confluence.component.pug',
    styleUrls: ['confluence.component.scss']
})
export class ConfluenceComponent implements OnInit {

    public dateFrom: Date;
    public dateTo: Date;

    private selectedSprint: any;

    private sprintsLoading: boolean;
    private loadingUsers: boolean;

    constructor(private confluenceService: ConfluenceService, private xlsxService: XLSXService, private dialog: MatDialog) {
        this.confluenceService = confluenceService;
        this.xlsxService = xlsxService;
    }

    ngOnInit() {
        $(() => {

        });
    }

    public onLoadClick() {
        this.sprintsLoading = true;
        this.confluenceService.loadMinskTeam().then((response: any) => {
            this.confluenceService.fetchData(this.dateFrom, this.dateTo).then((response: any) => {
                this.sprintsLoading = false;

                this.loadingUsers = true;
                this.confluenceService.loadUsers().then(response => {
                    this.loadingUsers = false;
                });
            });
        });
    }

    public onSelectSprint(sprint: any) {
        if(this.selectedSprint) {
            this.selectedSprint.selected = false;
        }

        if(this.selectedSprint != sprint) {
            this.selectedSprint = sprint;
            sprint.selected = true;
        } else {
            this.selectedSprint = null;
            sprint.selected = false;
        }
    }

    public handleSheetSelect(event: any) {
        var files = event.target.files;

        this.xlsxService.parseSheet(event.target.files[0]).then(workbook => {
            let dialogRef = this.dialog.open(ExportDialog, {
                width: '350px',
                data: {}
            });

            dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');
            });
        });
    }
}