import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import {Sheet, WorkBook } from "xlsx";
import {resolveDep} from "@angular/core/src/view/provider";
import {ConfluenceService} from "./ConfluenceService";
import {SprintModel} from "../models/SprintModel";
import {SprintUserModel} from "../models/SprintUserModel";


@Injectable()
export class XLSXService {

    public workbook: WorkBook;

    public sheetColumnNames: string[];

    constructor(
    ){

        var alphabet: string[] = 'abcdefghijklmnopqrstuvwxyz'.split('');
        this.sheetColumnNames = [];
        for (let i = -1; i < 5; i++) {
            for (let j = 0; j < alphabet.length; j++) {
                let value: string = (i >= 0 ? (alphabet[i] + alphabet[j]) : alphabet[j]);
                this.sheetColumnNames.push(value.toUpperCase());
            }
        }
    }

    public parseSheet(file: File): Promise<any> {
        return new Promise((resolve, reject) => {
            var reader = new FileReader();

            reader.onload = (e: any) => {
                this.arrayBuferLoaded(e);
                resolve(this.workbook);
            }
            reader.readAsArrayBuffer(file);
        });

    }

    private arrayBuferLoaded(e: any) {
        var  XLSX = require('xlsx');
        var arrayBuffer = e.target.result;

        this.workbook = XLSX.read(new Uint8Array(arrayBuffer), {type: 'array'});
    }

    public generateSheetAndSave(sheetName: string, confluenceService: ConfluenceService): Promise<any> {
        return new Promise((resolve, reject) => {
            let sheet: Sheet = this.workbook.Sheets[sheetName];

            let newSheet: {} = {};
            newSheet['!ref'] = sheet['!ref'];

            let listMemberIds: string[] = Object.keys(confluenceService.confluenceUsersProfiles);

            // Copy values from the first row
            let columnInd: number = 1;

            while (columnInd < 8 || this.getCellValue(sheet, columnInd, 1)) { // check columnInd < 8 index because there is an empty column before users list
                let cellInd = this.getCellInd(columnInd, 1);
                newSheet[cellInd] = sheet[cellInd];

                // populate columnInd to user objects
                if(columnInd >= 8) {
                    let userName = this.getCellValue(sheet, columnInd, 1);

                    for (let i = 0; i < listMemberIds.length; i++) {
                        let userInfo: object = confluenceService.confluenceUsersProfiles[listMemberIds[i]];
                        if (userName.toLowerCase().lastIndexOf(userInfo['displayName'].toLowerCase()) >= 0) {
                            userInfo['sheetColumnInd'] = columnInd;
                        }
                    }
                }

                columnInd++;
            }

            let lastUserColumnInd: number = columnInd + 2;

            let listSprints: SprintModel[] = confluenceService.listSprints;
            listSprints.sort(this.sortByStartDate);

            let listMissedInSheetUsers: string[] = []; // collect users which are missed in the template sheet

            lastUserColumnInd += 2; // make a gap between existing in sheet users and non-existing

            // fill sprints info to sheet
            for (let i = 0; i < listSprints.length; i++) {
                let sprint: SprintModel = listSprints[i];

                let rowInd = i + 3;
                newSheet[this.getCellInd(1, rowInd)] = {v: sprint.mission, t: 's'};
                newSheet[this.getCellInd(2, rowInd)] = {v: sprint.title, t: 's'};
                newSheet[this.getCellInd(3, rowInd)] = {v: 'development', t: 's'};
                newSheet[this.getCellInd(4, rowInd)] = {v: sprint.startDate.value, t: 'd'};
                newSheet[this.getCellInd(5, rowInd)] = {v: sprint.endDate.value, t: 'd'};


                // fill sprint members working hours
                for (let j = 0; j < sprint.fullTimeMembers.length; j++) {
                    let user: SprintUserModel = sprint.fullTimeMembers[j];
                    let userInfo: object = confluenceService.confluenceUsersProfiles[user.userKey];

                    if (userInfo.hasOwnProperty('sheetColumnInd')) {
                        newSheet[this.getCellInd(userInfo['sheetColumnInd'], rowInd)] = {v: user.workingDays * 8, t: 'n'};
                    } else {
                        lastUserColumnInd++;

                        userInfo['sheetColumnInd'] = lastUserColumnInd;
                        newSheet[this.getCellInd(lastUserColumnInd, 1)] = {v: userInfo['displayName'], t: 's'}; // write user name to the first row
                        newSheet[this.getCellInd(lastUserColumnInd, rowInd)] = {v: user.workingDays * 8, t: 'n'};

                        listMissedInSheetUsers.push(userInfo['displayName']); // save missed user
                    }
                }
            }

            newSheet['!ref'] = 'A1:' + this.getCellInd(lastUserColumnInd + 1, listSprints.length + 10); // set filled cells in sheet

            let wb: {} = {};
            wb['SheetNames'] = [sheetName];
            wb['Sheets'] = {};
            wb['Sheets'][sheetName] = newSheet;

            this.saveFile(wb);

            if (listMissedInSheetUsers.length == 0) {
                resolve();
            } else {
                reject(listMissedInSheetUsers); // show on UI which users weren't added to the Sheet
            }
        });
    }

    public saveFile(wb: {}): void {
        var  XLSX = require('xlsx');
        var wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };

        var wbout = XLSX.write(wb, wopts);

        function s2ab(s: any) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        }

        /* the saveAs call downloads a file on the local machine */
        var FileSaver = require('file-saver');
        FileSaver.saveAs(new Blob([s2ab(wbout)], {type: 'application/octet-stream'}), 'Resource time valuation report.xlsx');
    }

    private getCellValue(sheet: Sheet, columnInd: number, rowInd: number): string {
        let cellInd = this.getCellInd(columnInd, rowInd);
        return sheet[cellInd] && sheet[cellInd].hasOwnProperty('v') ? sheet[cellInd].v : null;
    }

    private getCellInd(columnInd: number, rowInd: number): string {
        return this.sheetColumnNames[columnInd - 1] + rowInd;
    }

    private sortByStartDate(a: object, b: object): number {
        let aDate: Date = a['startDate'].value as Date;
        let bDate: Date = b['startDate'].value as Date;

        if (aDate < bDate)
            return -1;
        if (aDate > bDate)
            return 1;
        return 0;
    }
}