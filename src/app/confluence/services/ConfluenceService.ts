import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import {Requests} from '../../core/modules/requests/Requests';
import {DateModel} from '../models/DateModel';
import {SprintModel} from "../models/SprintModel";
import {SprintUserModel} from "../models/SprintUserModel";

interface ISprintsMap {
    [id: string]: SprintModel;
}

@Injectable()
export class ConfluenceService {

    public dateFrom: Date;
    public dateTo: Date;

    public onlyMinskFlag: boolean = true;

    public confluenceUsersProfiles: {}; // {userKey: userProfileObj}

    public pdtSprintsDashboardID: string = '2556158';
    public futurePDTSprintsID: string = '65896538';
    public archievedPDTSprints2017: string = '124602708';
    public activePDTSprintsID: string = '46170128';

    public minskTeamPageID: string = '9536231';

    public sprintPagesMap: ISprintsMap;
    public listSprints: SprintModel[];
    public minskTeamIDs: {};

    public numberUsersToLoad: number = 0;
    public numberLoadedUsers: number = 0;

    constructor(
        private requests: Requests
    ){
        this.confluenceUsersProfiles = new Object();
        this.sprintPagesMap = {};
        this.listSprints = [];
        this.minskTeamIDs = new Object();
    }

    public fetchData(from: Date, to: Date): Promise<{}> {
        return new Promise((resolve, reject) => {

            this.dateTo = new Date(to); // convert to Date object
            this.dateFrom = new Date(from);
            this.listSprints.splice(0, this.listSprints.length);

            // 359432310
            // 334299137
            this.processPage([this.pdtSprintsDashboardID]).then(response => { // loads pages recursively
                resolve(response);
            });
        });
    }

    public getConfluencePageChilds(id: String): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.requests.getConfluencePageChilds(id).then(response => {
                if(response && response.hasOwnProperty('results')) {
                    resolve(response);
                } else {
                    reject();
                }
            });
        });
    }

    private processPage(pageIds: string[]): Promise<{}> {
        return new Promise((resolve, reject) => {
            if(pageIds.length === 0) {
                resolve();
                return;
            }

            this.getPageStorageContent(pageIds[0]).then(response => {
                let isSprintPage = response;

                if (isSprintPage) { // response=true if the page is a Sprint page

                    if(pageIds.length === 1) {
                        resolve(response);
                    }

                    pageIds.splice(0, 1);
                    this.processPage(pageIds).then(response => {
                        resolve(response);
                    });
                } else { // it's not a sprint page, get it's childs
                    this.getConfluencePageChilds(pageIds[0]).then((response: any) => {
                        let childPages: any[] = this.wrapToArray(response.results);
                        let childPageIds: string[] = [];
                        childPages.forEach(item => {

                            if (item.title.indexOf('Archived PDT Sprints') >= 0) { // check if Archieved PDT sprints apply selected year
                                if (item.title.indexOf(this.dateFrom.getFullYear()) >= 0 ||
                                    item.title.indexOf(this.dateTo.getFullYear()) >= 0)
                                {
                                    childPageIds.push(item.id);
                                }
                            } else if (item.id != '65896538') { // exclude Future PDT Sprints
                                childPageIds.push(item.id);
                            }
                        });

                        pageIds.splice(0, 1); // remove ID's as it's sprint page, so don't need to get it's child pages

                        this.processPage(childPageIds.concat(pageIds)).then(response => {
                            resolve(response);
                        });
                    });


                }
            });
        });
    }

    public getPageStorageContent(pageId: string): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.requests.getPageBodyStorageContent(pageId).then(response => {
                let bodyString = response.body.storage.value;
                console.log('Page: ' + response.title + ', is sprint: ' + this.isPageSprint(bodyString));

                if (this.isPageSprint(bodyString)) { // check if the page is a Sprint page
                    let xmlDoc = this.transformPageToXML(bodyString);
                    // debugger;
                    let dates = xmlDoc.querySelectorAll("time");
                    let startDate: DateModel = new DateModel(dates[0].getAttribute("datetime"));
                    let endDate: DateModel = new DateModel(dates[1].getAttribute("datetime"));

                    // check if sprint overlaps the selected period
                    if ((startDate.value >= this.dateFrom && startDate.value <= this.dateTo && endDate.value >= this.dateFrom && endDate.value <= this.dateTo) ||  // case 1:  [ < > ]
                        (startDate.value <= this.dateFrom && startDate.value <= this.dateTo && endDate.value >= this.dateFrom && endDate.value >= this.dateTo) || // case 2:  < [  ] >
                        (startDate.value >= this.dateFrom && startDate.value <= this.dateTo && endDate.value >= this.dateFrom && endDate.value >= this.dateTo) || // case 3:  [ <  ] >
                        (startDate.value <= this.dateFrom && startDate.value <= this.dateTo && endDate.value >= this.dateFrom && endDate.value <= this.dateTo))   // case 4:  < [  > ]
                    {
                        let workingDays: number = DateModel.calculateWorkingDaysInRange(startDate.value < this.dateFrom ? this.dateFrom : startDate.value,
                                                                                endDate.value > this.dateTo ? this.dateTo : endDate.value);

                        let sprint: SprintModel = new SprintModel();
                        sprint.id = pageId;
                        sprint.title = response.title;
                        sprint.startDate = startDate;
                        sprint.endDate = endDate;
                        sprint.workingDays = workingDays;

                        this.parseMembers(xmlDoc, sprint);

                        if (sprint.id == '197001749') {
                            // debugger;
                        }
                    }

                    resolve(true); // it's a sprint page
                } else {
                    resolve(false); // it's not a sprint page
                }

            });
        });
    }

    private isPageSprint(bodyString: string): boolean {
        return bodyString.indexOf('Create a Retro Page') >= 0;
    }

    private parseMembers(xmlDoc: any, sprint: SprintModel): void {
        let listFirstCells = xmlDoc.querySelectorAll('tr > *:first-child'); // get list first cell of each table rows
        let listProcessedUsers: SprintUserModel[] = [];


        for (let i = 0; i < listFirstCells.length; i++) {
            let cell = listFirstCells[i];

            let thHTMLString = cell.innerHTML.toLowerCase();
            let listKeyWords: string[] = ['full-time members', 'participants', 'test team', 'automation', 'developers'];
            let containFullTimeMembers = false;

            // check if current TH block contains data about full-time members
            listKeyWords.forEach(keyWord => {
                if (thHTMLString.indexOf(keyWord) >= 0) {
                    containFullTimeMembers = true;
                }
            });

            if (containFullTimeMembers) { // get sprint members
                let listUsersHTML = cell.parentElement.querySelectorAll('user');

                for (let j = 0; j < listUsersHTML.length; j++) {
                    let userModel: SprintUserModel = this.parseUserData(listUsersHTML[j], sprint);

                    if ((this.onlyMinskFlag && this.minskTeamIDs.hasOwnProperty(userModel.userKey)) || !this.onlyMinskFlag) {
                        listProcessedUsers.push(userModel);
                    }
                }
            } else if (thHTMLString.indexOf('mission') > -1) { // fill sprint mission
                let missionContainer = cell.parentElement.querySelector('span');
                sprint.mission = missionContainer != null ? missionContainer.textContent : '';
            } else if (
                thHTMLString.indexOf('vacation') > -1 ||
                thHTMLString.indexOf('holiday') > -1 ||
                thHTMLString.indexOf('unplanned') > -1 ||
                thHTMLString.indexOf('sick') > -1
            ) {
                this.parseSicksVacationsHolidays(cell, sprint, listProcessedUsers);
            }
        }


        sprint.fullTimeMembers = listProcessedUsers;

        if (sprint.fullTimeMembers.length > 0) {
            this.sprintPagesMap[sprint.id] = sprint;
            this.listSprints.push(this.sprintPagesMap[sprint.id]);
        }
    }

    private parseSicksVacationsHolidays(cell, sprint: SprintModel, listProcessedUsers: SprintUserModel[]): void {
        let listRows = cell.parentElement.querySelectorAll('p');

        for (let i = 0; i < listRows.length; i++) {
            let row = listRows[i];

            let userHTML = row.querySelector('user');

            // parse user vacation or day-off
            if (userHTML) {
                let userKey = userHTML.getAttribute('userkey');
                let user: SprintUserModel = listProcessedUsers.find(u => { return u.userKey === userKey; });
                if (!user) {
                    console.log('User in Vacation/Holidays tab not found as full-time member! User id: ' + userKey + ', sprint: ' + sprint.title);
                    continue;
                }

                let dates = row.querySelectorAll('time');

                if (dates.length === 1) { // single day-off or sick leave
                    let holiday = new Date(dates[0].getAttribute('datetime'));
                    if (holiday >= this.dateFrom && holiday <= this.dateTo) {
                        user.workingDays -= 1;
                    }
                } else if (dates.length === 2) { // range of dates (vacations, many day-offs, sick leaves)
                    let dateFrom = new Date(dates[0].getAttribute('datetime'));
                    let dateTill = new Date(dates[1].getAttribute('datetime'));

                    if (dateFrom < this.dateFrom) {
                        dateFrom = this.dateFrom;
                    }

                    if (dateTill > this.dateTo) {
                        dateTill = this.dateTo;
                    }

                    if (dateFrom < dateTill) {
                        user.workingDays -= DateModel.calculateWorkingDaysInRange(sprint.startDate.value < dateFrom ? dateFrom : sprint.startDate.value,
                                                                                  sprint.endDate.value > dateTill ? dateTill : sprint.endDate.value);
                    }
                }
            } else { // parse public holiday

                let rowText = row.innerHTML.toLowerCase();
                let isMinsk = rowText.indexOf('belarus') > -1 || rowText.indexOf('minsk') > -1;
                let isMovedHoliday = rowText.indexOf('moved') > -1;
                let dates = row.querySelectorAll('time');

                if (!isMinsk || dates.length === 0) {
                    continue;
                }

                if (isMovedHoliday) { // working day which was moved by holiday
                    let workingDay = new Date(dates[1].getAttribute('datetime'));

                    if (workingDay >= this.dateFrom && workingDay <= this.dateTo) {
                        sprint.workingDays += 1;

                        listProcessedUsers.forEach((user: SprintUserModel) => {
                            if (user.workFrom.value <= workingDay && workingDay <= user.workTill.value) {
                                user.workingDays += 1;
                            }
                        });
                    }
                } else { // holiday dates
                    if (dates.length === 1) { // single holiday
                        let holiday = new Date(dates[0].getAttribute('datetime'));


                        if (holiday >= this.dateFrom && holiday <= this.dateTo) {
                            sprint.workingDays -= 1;
                            listProcessedUsers.forEach((user: SprintUserModel) => {
                                if (user.workFrom.value <= holiday && holiday <= user.workTill.value) {
                                    user.workingDays -= 1;
                                }
                            });
                        }
                    } else if (dates.length === 2) { // multi holidays
                        let dateFrom = new Date(dates[0].getAttribute('datetime'));
                        let dateTill = new Date(dates[1].getAttribute('datetime'));



                        if (dateFrom < this.dateFrom) {
                            dateFrom = this.dateFrom;
                        }

                        if (dateTill > this.dateTo) {
                            dateTill = this.dateTo;
                        }

                        if (dateFrom < dateTill) {
                            let holidaysNum = DateModel.calculateWorkingDaysInRange(dateFrom, dateTill);
                            sprint.workingDays -= holidaysNum;

                            listProcessedUsers.forEach((user: SprintUserModel) => {
                                user.workingDays -= DateModel.calculateWorkingDaysInRange(user.workFrom.value < dateFrom ? dateFrom : user.workFrom.value,
                                    user.workTill.value > dateTill ? dateTill : user.workTill.value);
                            });
                        }
                    }
                }
            }
        }
    }

    private parseUserData(userHTML, sprint: SprintModel): SprintUserModel {
        let userModel: SprintUserModel = new SprintUserModel();
        userModel.userKey = userHTML.getAttribute('userkey');

        let userContainer = userHTML.parentElement.parentElement;

        if (userContainer.querySelector(':scope > time')) { // check if <time> tag is first child of container
            let userContainerString: string = userContainer.innerHTML ? userContainer.innerHTML.toLowerCase() : '';
            let userDateTime = userContainer.querySelector(':scope > time').getAttribute('datetime');

            if (userContainerString.indexOf('joined') >= 0 || userContainerString.indexOf('from') >= 0) {
                userModel.workFrom = new DateModel(userDateTime);
                userModel.workingDays = DateModel.calculateWorkingDaysInRange(userModel.workFrom.value < this.dateFrom ? this.dateFrom : userModel.workFrom.value,
                                                                            sprint.endDate.value > this.dateTo ? this.dateTo : sprint.endDate.value);
            } else if (userContainerString.indexOf('till') >= 0) {
                userModel.workTill = new DateModel(userDateTime);
                userModel.workingDays = DateModel.calculateWorkingDaysInRange(sprint.startDate.value < this.dateFrom ? this.dateFrom : sprint.startDate.value,
                                                                            userModel.workTill.value > this.dateTo ? this.dateTo : userModel.workTill.value);
            }
        } else {
            userModel.workingDays = sprint.workingDays;
        }

        if (!userModel.workFrom) {
            userModel.workFrom = sprint.startDate;
        }

        if (!userModel.workTill) {
            userModel.workTill = sprint.endDate;
        }

        return userModel;
    }

    public loadMinskTeam(): Promise<{}> {
        return new Promise((resolve, reject) => {
            this.requests.getPageBodyStorageContent(this.minskTeamPageID).then(response => {
                let bodyString = response.body.storage.value;
                let xmlDoc = this.transformPageToXML(bodyString);

                xmlDoc.querySelectorAll('user').forEach((item: any) => {
                    this.minskTeamIDs[item.getAttribute('userkey')] = "";
                });

                resolve(this.minskTeamIDs);
            });
        });
    }

    public loadUsers(): Promise<{}> {
        return new Promise((resolve, reject) => {
            let listUserKeys = [];
            for (let i = this.listSprints.length - 1; i >= 0; i--) {

                let sprintFulltimeMembers = this.listSprints[i].fullTimeMembers;
                for (let j = sprintFulltimeMembers.length - 1; j >= 0; j--) {
                    let userKey = sprintFulltimeMembers[j].userKey;
                    if (listUserKeys.indexOf(userKey) < 0) {
                        listUserKeys.push(userKey);
                    }
                }
            }

            this.numberUsersToLoad = listUserKeys.length;
            this.numberLoadedUsers = 0;

            if (listUserKeys.length != 0) {
                this.loadUsersSequence(listUserKeys).then(response => { // loads users recursively
                    resolve(response);
                });
            } else {
                resolve();
            }

        });
    }

    private loadUsersSequence(listUserKeys: string[]): Promise<{}> {
        return new Promise((resolve, reject) => {
            let userKey = listUserKeys.pop();
            this.numberLoadedUsers++;

            if (this.confluenceUsersProfiles.hasOwnProperty(userKey)) { // user already loaded
                if (listUserKeys.length > 0) {
                    this.loadUsersSequence(listUserKeys).then(response => { // load next user
                        resolve(response);
                    });
                } else {
                    resolve(this.confluenceUsersProfiles[userKey]); // all users loaded
                }

            } else { // user isn't loaded
                this.requests.getUserByKey(userKey).then(response => {
                    this.confluenceUsersProfiles[userKey] = response;

                    if(listUserKeys.length > 0) {
                        this.loadUsersSequence(listUserKeys).then(response => { // load next user
                            resolve(response);
                        });
                    } else {
                        resolve(response); // all users loaded
                    }

                });
            }
        });
    }

    private transformPageToXML(xmlString: string): any {
        xmlString = xmlString.replace(/ac:/g, '');
        xmlString = xmlString.replace(/ri:/g, '');
        xmlString = xmlString.replace(/&nbsp;/g, '');
        xmlString = xmlString.replace(/&ldquo;/g, '');
        xmlString = xmlString.replace(/&rdquo;/g, '');
        xmlString = xmlString.replace(/&gt;/g, '');


        let parser = new DOMParser();
        return parser.parseFromString(xmlString, 'text/xml');
    }

    private wrapToArray(source:any): Array<any> {
        return Array.isArray(source) ? source : [source];
    }
}
