import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';

import { ConfluenceRoutingModule, routedComponents } from './confluence-routing.module';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ExportDialog} from '../confluence/export-dialog/ExportDialog';

@NgModule({
    imports: [CommonModule, ConfluenceRoutingModule, MaterialModule, FormsModule],
    declarations: [routedComponents, ExportDialog],
    entryComponents: [ExportDialog],
    providers: []
})

export class ConfluenceModule {
}
