import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Component, Inject } from '@angular/core';

import { ConfluenceService } from '../services/ConfluenceService';
import { XLSXService } from '../services/XLSXService';
import {Requests} from '../../core/modules/requests/Requests';
import {Sheet, WorkBook, XLSX$Utils} from "xlsx";

@Component({
    selector: 'export-dialog',
    templateUrl: './export.dialog.pug',
    styleUrls: ['./export.dialog.scss']
})
export class ExportDialog {

    private selectedValue: string;

    private missedUsers: string;

    constructor(private confluenceService: ConfluenceService, private xlsxService: XLSXService, public dialogRef: MatDialogRef<ExportDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.missedUsers = '';
    }

   /* onNoClick(): void {
        this.dialogRef.close();
    }*/

    selectionChanged(): void {

    }

    public saveToXlsx(): void {
        this.xlsxService.generateSheetAndSave(this.selectedValue, this.confluenceService).then(resolve => {
            this.dialogRef.close();
        }).catch((rejectedUsers: string[]) => {
            this.missedUsers = '';
            for (let i = 0; i < rejectedUsers.length; i++) {
               this.missedUsers += rejectedUsers[i];

               if(i < rejectedUsers.length-1) this.missedUsers += ', ';
            }
        });
    }
}