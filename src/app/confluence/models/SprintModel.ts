import {DateModel} from "./DateModel";
import {SprintUserModel} from "./SprintUserModel";

export class SprintModel {
    id: string;
    title: string;
    startDate: DateModel;
    endDate: DateModel;
    workingDays: number;
    mission: string;
    fullTimeMembers: SprintUserModel[];
}