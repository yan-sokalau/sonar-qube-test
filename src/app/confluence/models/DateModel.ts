/**
 * Created by Yan.Sokalau on 08.11.2017.
 */
export class DateModel {

    month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    value: Date;
    displayDate: string;


    constructor(dateStr: string) {
        this.value = new Date(dateStr);

        this.displayDate = this.month_names_short[this.value.getMonth()] + ' ' + this.value.getDate() + ', ' + this.value.getFullYear();
    }

    static calculateWorkingDaysInRange(fromDate: Date, tillDate: Date): number {

        var numberOfWorkingDays = 0;
        for (let d = new Date(fromDate); d <= tillDate; d.setDate(d.getDate() + 1)) {
            if (this.isDateWorkingDay(d)) {
                numberOfWorkingDays++;
            }
        }

        return numberOfWorkingDays;
    }

    static isDateWorkingDay(date: Date): boolean {
        return date.getDay() != 0 && date.getDay() != 6;
    }

    toString(): string {
        return this.displayDate;
    }
}