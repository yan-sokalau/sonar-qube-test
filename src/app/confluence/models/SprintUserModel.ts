import {DateModel} from "./DateModel";

export class SprintUserModel {
    userKey: string;
    workFrom: DateModel;
    workTill: DateModel;
    workingDays: number;
}