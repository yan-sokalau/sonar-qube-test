import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {MaterializeComponent} from './angular-compnents/materialize.component';

const routes: Routes = [
    {
        path: '',
        component: MaterializeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MaterializeRoutingModule {
}

export const routedComponents = [
    MaterializeComponent
];