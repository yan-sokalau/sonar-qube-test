import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';

import { MaterializeRoutingModule, routedComponents } from './materialize-routing.module';
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule, MaterializeRoutingModule, MaterialModule],
    declarations: [routedComponents],
    providers: []
})

export class MaterializeModule { }
