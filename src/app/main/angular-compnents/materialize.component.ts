import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'shop',
    templateUrl: 'materialize.component.pug',
    styleUrls: ['materialize.component.scss']
})
export class MaterializeComponent implements OnInit {
    private options: Array<Object> = [
        {value: '1', viewValue: 'First'},
        {value: '2', viewValue: 'Second'},
        {value: '3', viewValue: 'Third'}
      ];
    disabled = true;
    constructor() {}

    ngOnInit() { }
}