import { AppConfig } from './app.config';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

/* App Root */
import { AppComponent } from './app.component';

/* Feature Modules */
import { CoreModule } from './core/core.module';

/* Routing Module */
import { AppRoutingModule } from './app-routing.module';
import { APP_BASE_HREF } from '@angular/common';
import {ConfigService} from './core/modules/config/config.servise';
import { HttpModule } from '@angular/http';
import {EventsService} from './core/modules/utils/event.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
	imports: [
		BrowserModule,
		/* Core Module */
		CoreModule,
		AppRoutingModule,
		HttpModule,
		BrowserAnimationsModule
	],
	declarations: [AppComponent],
	providers: [{ provide: AppConfig, useValue: process.env.APP_CONFIG }, { provide: APP_BASE_HREF, useValue: '/' + (window.location.pathname.split('/')[1] || '')}, ConfigService, EventsService],
	bootstrap: [AppComponent]
})
export class AppModule { }
