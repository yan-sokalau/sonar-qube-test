import { Component } from '@angular/core';


import '../public/scss/styles.scss';

@Component({
	selector: 'app',
	templateUrl: './app.component.pug',
	styleUrls: ['app.component.scss']
})
export class AppComponent {
	appName = 'Base project';
}
