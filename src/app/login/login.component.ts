import {AuthenticationService} from '../core/service/authentication.service';
import { Component } from '@angular/core';
import {EventsService} from '../core/modules/utils/event.service';
import {GlobalEvents} from '../core/modules/utils/global.events';
import { Router } from '@angular/router';

@Component({
	selector: 'login',
	templateUrl: './login.component.pug',
    styleUrls: ['login.component.scss']
})

export class LoginComponent {
    public userId: string = 'andrew';
    public password: string = '123456aA!';
    public display: boolean = false;
    public showLoader: boolean = false;

    constructor(
                public authenticationService: AuthenticationService,
                private eventsAggregator: EventsService,
                private router: Router
    ) {
        this.display = true;
        eventsAggregator.on(GlobalEvents.AUTH_SUCCESS, () => {
            this.router.navigate(['/components']);
        });

    }


    public auth(): void {
        let validationHasPassed = true;
        let noCloudRestrictions = true;
        this.showLoader = true;
            this.authenticationService.authenticate(this.userId, this.password).then(
                (response: any) => {
                    console.log(response);
                }, (error: any) => {
                    this.showLoader = false;
                }
            );
    }
}
