import {AuthenticationService} from '../core/service/authentication.service';
import { Requests } from '../core/modules/requests/Requests';
import { NgModule } from '@angular/core';
import { LoginRoutingModule, routedComponents } from './login-routing.module';
import { MaterialModule } from '../material.module';


@NgModule({
    imports: [LoginRoutingModule, MaterialModule],
    declarations: [routedComponents],
    providers: [Requests, AuthenticationService]
})

export class LoginModule { }