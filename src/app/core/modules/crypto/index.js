define([
    'exports'
    , 'crypto-js/core'
    , 'fingerprintjs'
    , './rsa'
    , 'crypto-js/aes'
    , 'crypto-js/md5'
    , 'crypto-js/sha256'
    , 'crypto-js/mode-ecb'
], function (exports, CryptoJS, Fingerprint) {
    var Cookie = {
        setCookie: function (key, value, expireTime) {
            var expires = new Date();
            if (!expireTime)
                expires.setTime(expires.getTime() + (30 * 1 * 24 * 60 * 60 * 1000));
            else
                expires.setTime(expires.getTime() + expireTime);
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        },

        getCookie: function (key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        },

        delCookie: function (key) {
            document.cookie = key + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT";
        }
    };

    CryptoJS.enc.u8array = {
        stringify: function (wordArray) {
            var words = wordArray.words;
            var sigBytes = wordArray.sigBytes;

            var u8 = new Int8Array(sigBytes);
            for (var i = 0; i < sigBytes; i++) {
                var byte = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
                u8[i] = byte;
            }

            return u8;
        },

        parse: function (u8arr) {
            var len = u8arr.length;
            var words = [];
            for (var i = 0; i < len; i++) {
                words[i >>> 2] |= (u8arr[i] & 0xff) << (24 - (i % 4) * 8);
            }

            return CryptoJS.lib.WordArray.create(words, len);
        }
    };

    var SettingsProvider = function(cfg){
        this.cfg = cfg;
    };

    SettingsProvider.prototype.getCertificateId = function () {
        var certificateId = this.cfg.certificate.id;
        if(this.cfg.env === 'prod')
        {
            certificateId = this.cfg.prod_certificate.id;
        }
        return certificateId;
    };

    SettingsProvider.prototype.getPublicKey = function () {
        var key = this.cfg.keys.public;
        if(this.cfg.env === 'prod')
        {
            key = this.cfg.prod_keys.public;
        }
        return key;
    };

    SettingsProvider.prototype.getPrivateKey = function () {
        var key = this.cfg.keys.private;
        if(this.cfg.env === 'prod')
        {
            key = this.cfg.prod_keys.private;
        }
        return key;
    };

    var Crypto = function (salt, pwd, userId, cfg) {
        this.cfg = cfg;
        this.settingsProvider = new SettingsProvider(this.cfg);
        var key = CryptoJS.SHA256(window.location.host + userId).toString();
        var cdvToken = Cookie.getCookie(key);
        if (!cdvToken) {
            cdvToken = "";
        } else {
            try {
                var decrypt = new JSEncrypt();
                decrypt.setPrivateKey(this.settingsProvider.getPrivateKey());
                cdvToken = decrypt.decrypt(cdvToken);
                if (!cdvToken) {
                    var key = CryptoJS.SHA256(window.location.host + userId).toString();
                    Cookie.delCookie(key);
                    cdvToken = "";
                }
            } catch (e) {
                var key = CryptoJS.SHA256(window.location.host + userId).toString();
                Cookie.delCookie(key);
                cdvToken = "";
            }
        }
        var identifierForVendor = new Fingerprint({screen_resolution: true}).get();
        this.application = this.cfg.name;
        this.fingerPrint =
        {
            "otcData": {
                "CDvToken": cdvToken,
                "deviceInfo": [
                    {
                        "entry": [
                            {"string": ["identifierForVendor", identifierForVendor.toString()]},
                            {"string": ["application", this.application]},
                            {"string": ["version", "0.0.59"]}
                        ]
                    }
                ]
            }
        };


        this.salt = salt;

        //this.certificateId = this.getCertificateId();

        this.pwd = pwd;

    };

    Crypto.prototype.getCertificateId = function () {
        return this.settingsProvider.getCertificateId();
    };

    Crypto.prototype.getOTC1MD5 = function () {
        var OTC1MD5 = CryptoJS.MD5(encodeURIComponent(JSON.stringify(this.fingerPrint))).toString();
        return OTC1MD5;
    };

    Crypto.prototype.getRS1MD5 = function () {
        var RS1MD5 = CryptoJS.MD5(this.salt).toString();
        return RS1MD5;
    };

    Crypto.prototype.getUC1MD5 = function () {
        var UC1MD5 = CryptoJS.MD5(this.pwd).toString();
        return UC1MD5;
    };

    Crypto.prototype.getUC2MD5 = function () {
        var UC2MD5 = CryptoJS.MD5(this.getOTC1MD5() + this.getUC1MD5()).toString();
        return UC2MD5;
    };

    Crypto.prototype.getOTC1AES = function () {
        var dataToEncrypt = JSON.stringify(this.fingerPrint);
        var key = CryptoJS.MD5(this.getUC2MD5() + this.salt).toString();
        encryptionKey = CryptoJS.enc.Utf8.parse(key);
        var OTC1AES = CryptoJS.AES.encrypt(dataToEncrypt, encryptionKey, {mode: CryptoJS.mode.ECB});
        //console.log("OTC1AE: "+CryptoJS.AES.decrypt(OTC1AES, encryptionKey, { mode: CryptoJS.mode.ECB}).toString(CryptoJS.enc.Utf8));
        return OTC1AES;
    };

    Crypto.prototype.getOTC2AES = function () {
        var dataToEncrypt = this.getOTC1MD5() + this.getRS1MD5();
        //console.log("Data to encrypt: "+dataToEncrypt);
        var key = this.getUC1MD5();
        encryptionKey = CryptoJS.enc.Utf8.parse(key);
        var OTC2AES = CryptoJS.AES.encrypt(dataToEncrypt, encryptionKey, {mode: CryptoJS.mode.ECB});
        //console.log("OTC2AE: "+CryptoJS.AES.decrypt(OTC2AES, encryptionKey, { mode: CryptoJS.mode.ECB}).toString(CryptoJS.enc.Utf8));
        return OTC2AES;
    };

    Crypto.prototype.getEOTC1 = function () {

        var OTC2AES = this.getOTC2AES();
        //var OTC2AES = CryptoJS.enc.Utf8.parse('hello');
        var encrypt = new JSEncrypt();
        encrypt.setPublicKey(this.settingsProvider.getPublicKey());
        var OTC2AESbytes = CryptoJS.enc.u8array.stringify(OTC2AES.ciphertext);
        var EOTC1 = encrypt.encrypt(OTC2AESbytes, false);
        //      var decrypt =  new JSEncrypt();
        //      decrypt.setPrivateKey(this.pemkeys.private);
        //      decrypt.decrypt(EOTC1);
        return EOTC1;
    };

    Crypto.prototype.getCrypto = function () {

        var EOTC1 = this.getEOTC1();
        var OTC1AES = this.getOTC1AES();
        return {
            "credentialData": this.getCertificateId() + EOTC1,
            "otcData": OTC1AES.ciphertext.toString(),
            "application": this.application
        };
    };

    var SimpleCrypto = function(cfg){
        this.cfg = cfg;
        this.settingsProvider = new SettingsProvider(this.cfg);
    };

    SimpleCrypto.prototype.getCertificateId = function () {
        return this.settingsProvider.getCertificateId();
    };


    SimpleCrypto.prototype.rsaDecrypt = function (encryptedData) {
        var decryptedData;

        try {
            var decrypt = new JSEncrypt();
            decrypt.setPrivateKey(this.settingsProvider.getPrivateKey());
            decryptedData = decrypt.decrypt(encryptedData);

        } catch (e) {
            console.log(e);
        }

        return decryptedData;
    };

    SimpleCrypto.prototype.rsaEncrypt = function (dataToEncrypt) {
        var encryptedData;

        try {
            var encrypt = new JSEncrypt();
            encrypt.setPublicKey(this.settingsProvider.getPublicKey());
            //var dataToEncryptBytes = CryptoJS.enc.u8array.stringify(dataToEncrypt);
            encryptedData = encrypt.encrypt(dataToEncrypt, true);
        } catch (e) {
            console.log(e);
        }

        return encryptedData;
    };

    exports.Crypto = Crypto;
    exports.SimpleCrypto = SimpleCrypto;
});
