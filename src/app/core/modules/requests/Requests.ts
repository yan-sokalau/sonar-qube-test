import {Request} from './Request'
import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {ConfigService} from '../config/config.servise';
import {EventsService} from '../utils/event.service';
import {ExceptionService}  from '../errors/exception.service';

@Injectable()
export class Requests extends Request {
    public constructor(public cfg: ConfigService, protected http: Http, private eventsAggregator: EventsService, protected exception: ExceptionService)  {
        super(cfg, http, eventsAggregator, exception);
    }

    // CONFLUENCE METHODS

    public getConfluencePageChilds(id: String): Promise<any> {
        let path = 'wiki/rest/api/content/'+id+'/child/page?limit=100';
        let data = {

        };
        let base64 = 'Basic '+btoa('yan.sokalau@capsilon.com:f3DE6878!');

        return this.get(path, null, 'json', {key: 'Authorization', value: base64.toString()}, 'https://capsilon.atlassian.net/');
    }

    public getPageBodyStorageContent(id: String): Promise<any> {
        let path = 'wiki/rest/api/content/'+id+'?expand=body.storage'
        let data = {

        };
        let base64 = 'Basic '+btoa('yan.sokalau@capsilon.com:f3DE6878!');

        return this.get(path, null, 'json', {key: 'Authorization', value: base64.toString()}, 'https://capsilon.atlassian.net/');
    }

    public getUserByKey(id: String): Promise<any> {
        let path = 'wiki/rest/api/user?key='+id;
        let data = {

        };
        let base64 = 'Basic '+btoa('yan.sokalau@capsilon.com:f3DE6878!');

        return this.get(path, null, 'json', {key: 'Authorization', value: base64.toString()}, 'https://capsilon.atlassian.net/');
    }

    // COMMON METHODS

    public validateApplicationAndSite(): Promise<any> {
        this.setValidateApplicationAndSiteUrl();
        let applicationName = this.applicationName;
        let applicationSecret = this.applicationSecret;
        let siteAddress = this.getSiteName();
        let path = 'HUB/services/rest/ApplicationService/validateApplicationAndSite';
        let data = {
            applicationName: applicationName,
            applicationSecret: applicationSecret,
            siteAddress: siteAddress
        };
        return this.post(path, data, null, null);
    }

    public clearAndGenerateApplicationTicketsCollection(userTicketId): Promise<any> {
        let path = 'ApplicationTicketInterface/clearAndGenerateApplicationTicketsCollection';
        let data = {
            applicationName: this.applicationName,
            applicationSecret: this.applicationName,
            noOfTickets: this.numOfTickets
        };
        return this.post(path, data, 'json', {key: 'userTicketId', value: userTicketId});
    }

    public generateApplicationTicketsCollection(userTicketId, num): Promise<any> {
        let path = 'ApplicationTicketInterface/generateApplicationTickets';
        let data = {
            applicationName: this.applicationName,
            applicationSecret: this.applicationName,
            noOfTickets: num
        };
        return this.post(path, data, 'json', {key: 'userTicketId', value: userTicketId});
    }

    public authenticate(siteId, principal, encryptedData, isNetworkUser: boolean = false): Promise<any> {
        let URL =
            isNetworkUser ? 'UserTicketInterface/authenticateGuestNetworkUser' : 'UserTicketInterface/authenticate';
        let data = {
            siteId: siteId,
            applicationName: encryptedData.application,
            principal: principal,
            otcData: encryptedData.otcData,
            credentialData: encryptedData.credentialData
        };
        return this.post(URL, data);
    }

    public logout(): Promise<any> {
        const URL = 'UserTicketInterface/logOut';
        const data = {
           // userTicketId: AuthenticationService['user'].getId
        };
        return this.get(URL, data);
    }


    private getCertificateId(): String {
        let certificateId = this.cfg.certificate.id;
        if (this.cfg.env === 'prod') {
            certificateId = this.cfg.production_certificate.id;
        }
        return certificateId;
    }

    public createOneTimeLaunchConfiguration(launchConfiguration: Object, userTicketId: string): Promise<any> {
        let URL = 'UserTicketInterface/createOneTimeLaunchConfiguration';

        launchConfiguration['oneTimeLaunchConfigurationRequest'].applicationName = this.applicationName;
        launchConfiguration['oneTimeLaunchConfigurationRequest'].certificateId = this.getCertificateId();

        return this.post(URL, JSON.stringify(launchConfiguration), 'json', {key: 'userTicketId', value: userTicketId},
            null, "application/json; charset=UTF-8");
    }

    public getOneTimeLaunchConfiguration(launchConfigurationId: Object): Promise<any> {
        let URL = 'UserTicketInterface/getOneTimeLaunchConfiguration';

        let data = {
            siteId: this.getSiteId,
            applicationName: this.applicationName,
            certificateId: this.getCertificateId(),
            launchConfigurationId: launchConfigurationId
        };
        return this.post(URL, data, 'json');
    }

    public validateApplicationTicket(ticketId: string): Promise<any> {
        const URL = 'ApplicationTicketInterface/validateApplicationTicket';
        let data = {
            applicationTicketId: ticketId
        }
        return this.get(URL, data, 'json');
    }
}
