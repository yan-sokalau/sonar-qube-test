import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {ConfigService} from '../config/config.servise';
import{ApplicationTicketsProvider} from '../application-tickets/ApplicationTicketsProvider';
import {EventsService} from '../utils/event.service';
import {ExceptionService}  from '../errors/exception.service';

interface Header {
    key: string;
    value: string;
}

export class Request {
    public static restPrefix: string;
    public static applicationTicketsWorker: ApplicationTicketsProvider;
    private static siteObject: any;
    private static siteId: any;
    public XHR: XMLHttpRequest;
    public applicationName: string;
    public applicationSecret: string;
    public baseURL;
    public directLaunch: boolean = false;
    public numOfTickets: number;

    constructor(public cfg: ConfigService, protected http: Http, eventsAggregator: EventsService, protected exception:  ExceptionService) {
        Request.restPrefix = this.cfg.api_prefix;
        this.applicationName = this.cfg.name;
        this.applicationSecret = this.cfg.secret;
        this.numOfTickets = this.cfg.tickets;
        Request.applicationTicketsWorker = new ApplicationTicketsProvider(eventsAggregator);
        this.setSiteObject();
    }

    public get(path: string, data: any = {}, dataType: string = 'json', header: Header = {key: null, value: null},
               baseURL: string = this.baseURL, contentType: string = null, ...restOfHeaders: Array<Header>): any {
        if (baseURL == null) {
            baseURL = this.baseURL;
        }
        const url = baseURL + path;

        return this.request('get', url, data, dataType, header, contentType, ...restOfHeaders);
    }

    public post(path: string, data: any = {}, dataType: string = 'json', header: Header = {key: null, value: null},
                baseURL: string = this.baseURL, contentType: string = null, ...restOfHeaders: Array<Header>): any {
        if (baseURL == null) {
            baseURL = this.baseURL;
        }

        const url = baseURL + path;
        return this.request('post', url, data, dataType, header, contentType, ...restOfHeaders);
    }

    public request(type: string, url: string, data: any = {}, dataType: string = 'json',
                   header: Header = {key: null, value: null}, contentType: string = null,
                   ...restOfHeaders: Array<Header>): any {
        let applicationTicketId = null;
        if (header && header.key === 'applicationTicketId') {
            return new Promise((resolve, reject) => {
                Request.applicationTicketsWorker.getNextApplicationTicketId().then((response) => {
                    header.value = response;
                    applicationTicketId = header.value;
                    if (!applicationTicketId) {
                        reject(null);
                    } else {
                        this.requestHelper(type, url, data, dataType, header, contentType,
                            restOfHeaders, applicationTicketId).then((response) => {
                                resolve(response);
                            },
                            (error) => {
                                reject(error);
                            });
                    }
                });
            });
        } else if (data && data.applicationTicketId) {
            return new Promise((resolve, reject) => {
                Request.applicationTicketsWorker.getNextApplicationTicketId().then(response => {
                    data.applicationTicketId = response;
                    applicationTicketId = data.applicationTicketId;
                    if (!applicationTicketId) {
                        reject(null);
                    } else {
                        this.requestHelper(type, url, data, dataType, header,
                            contentType, restOfHeaders, applicationTicketId).then((response) => {
                                resolve(response);
                            },
                            (error) => {
                                reject(error);
                            });
                    }
                });
            });
        } else if (typeof data === 'string') {
            return new Promise((resolve, reject) => {
                Request.applicationTicketsWorker.getNextApplicationTicketId().then((response) => {
                    data = data.replace('applicationTicketId=true', `applicationTicketId=${response}`);
                    applicationTicketId = response;
                    if (!applicationTicketId) {
                        reject(null);
                    } else {
                        this.requestHelper(type, url, data, dataType, header,
                            contentType, restOfHeaders, applicationTicketId).then(
                            (response) => {
                                resolve(response);
                            }, (error) => {
                                reject(error);
                            });
                    }
                });
            });
        } else {
            return this.requestHelper(type, url, data, dataType, header, contentType, restOfHeaders, null);
        }
    }

    public setBaseUrl(url): void {
        this.baseURL = url;
    }

    public setValidateApplicationAndSiteUrl(): void {
        const siteObject = Request.siteObject;
        const addressArr = siteObject.siteName.split('.');

        if (addressArr.length > 2) {
            addressArr.shift();
        }

        this.setBaseUrl(siteObject.protocol + '//' + addressArr.join('.') + '/');
    }

    public getSiteObject(): Object {
        return Request.siteObject;
    }

    public getSiteName(): string {
        return Request.siteObject.siteName;
    }

    public getSiteId(): string {
        return Request.siteId;
    }

    public setSiteId(siteId: string) {
        Request.siteId = siteId;
    }

    public setApplicationTickets(tickets: object[]): void {
        Request.applicationTicketsWorker.initApplicationTickets(tickets);
    }

    public getNextApplicationTicketProvider(): ApplicationTicketsProvider {
        return Request.applicationTicketsWorker;
    }

    public setApplicationName(): void {
        const applicationName = Request.applicationTicketsWorker.getApplicationName();
        if (applicationName !== this.applicationName) {
            this.applicationName = applicationName;
        }
    }

    public setDirectLaunch(): void {
        this.directLaunch = true;
    }

    public validateAplicationTicket(path, applicationTicketId: string, dataType: string = 'json', data: any = {},
                                    header: Header = {
                                        key: 'applicationTicketId',
                                        value: null
                                    }, contentType: string = null,
                                    ...restOfHeaders: Array<Header>): Promise<any> {
        const url = this.baseURL + path;
        header.value = applicationTicketId;
        return this.requestHelper('post', url, data, dataType, header, contentType, applicationTicketId, ...restOfHeaders);
    }

    private requestHelper(type: string, url: string, data: any = {}, dataType: string = 'json',
                          header: Header = {key: null, value: null}, contentType: string = null,
                          applicationTicketId,
                          ...restOfHeaders: Array<Header>): Promise<any> {
        let headers = new Headers();
        if (header && header.key) {
            headers.set(header.key, header.value);
        }
        let params  = data;
        let options = new RequestOptions({ headers: headers, params: params, method: type });


        return new Promise<any>((resolve, reject) => {
            Promise.all([
                this.httpRequest(url, options).catch(
                    (error) => {
                        this.exception.error(error.json().exception, error.url);
                        reject(error.json().exception);
                    }
                ),
                Promise.resolve(applicationTicketId)])
            .then(
                (response) => {
                    if(response[0]) {
                        resolve(response[0].json());
                    }
                    if(response[1].length){
                        Request.applicationTicketsWorker.unlockApplicationTicket(response[1]);
                    }
                }
            );
        });
    }

    private httpRequest(url, options): Promise<any>{
        return this.http.request(url,options).toPromise();
    }

    private setSiteObject(): void {
        let protocol;
        let siteName;
        let validateApplicationAndSiteURL: string = this.cfg.api as string;
        if (!validateApplicationAndSiteURL || validateApplicationAndSiteURL.length <= 1) {
            protocol = window.location.protocol;
            siteName = window.location.host;
        }
        else {
            protocol = validateApplicationAndSiteURL.split('//')[0];
            siteName = validateApplicationAndSiteURL.split('//')[1];
        }
        let addressArr = siteName.split('.');
        if ((siteName.indexOf('awh') !== -1) || /*Utils.getUrlParameter('siteName') ||*/ addressArr.length <= 2) {

            if (addressArr.length > 2) {
                addressArr.shift();
            }
            let urlParamSiteName = this.getUrlParameter('siteName');
            siteName = urlParamSiteName ? urlParamSiteName + '.' + addressArr.join('.') : null;

        }

        Request.siteObject = {
            protocol: protocol,
            siteName: siteName ? siteName.replace('/', '') : null
        }
    }

    public getUrlParameter(name): String {
        let results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window['location'].href);
        return results ? results[1] : undefined;
    }
}