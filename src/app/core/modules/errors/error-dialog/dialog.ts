import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'error-dialog',
  templateUrl: './dialog.pug',
})
export class ErrorDialog {
  private settings: Object;
  constructor(public dialogRef: MatDialogRef<ErrorDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.settings = this.data['errorSettings'];
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}