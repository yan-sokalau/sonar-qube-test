import {Errors} from './errors.config';
import { Injectable } from '@angular/core';
import {ErrorDialog} from './error-dialog/dialog';
import {MatDialog} from '@angular/material';


@Injectable()
export class ExceptionService {
    private currentDisplayedErrors: object = {};

    constructor(private configErrors: Errors, private dialog: MatDialog) {}

    public error(exception: object, url: string = null, callback?: () => any) {
        const modifiedException = this.interceptSpecialCases(exception, url);
        this.handleError(modifiedException);
    }


    private interceptSpecialCases(exception, url: string = null): object {
        if (url && url.length > 0) {
            const splitted = url.split('/');
            const apiName = splitted[splitted.length - 1];
            if (apiName) {
                switch (apiName) {
                    case 'getOneTimeLaunchConfiguration':
                        exception.errorCode = 'GET_LAUNCH_CONFIGURATION_TWICE';
                        break;
                }
            }
        }
        return exception;
    }

    private getErrorSettings(exception) {
        let code = exception.errorCode ? exception.errorCode : exception.code,
            message = exception.errorMessage ? exception.errorMessage : exception.message,
            errorPropsReplacements = exception.errorPropsReplacements || false,
            err = this.configErrors.errors['API'][code] || this.configErrors.errors['API']["DEFAULT_ERROR"];
        if (err.use_cloud_message) {
            err.message = message;
        }

        if (err.skip) {
            return;
        }

        if (errorPropsReplacements) {
            Object.assign(err, errorPropsReplacements);
        }

        /****
         Add ability to change header of error by message
         &header_replacements is a specific field for this
         */
        if (err.header_replacements) {
            for (let key in err.header_replacements) {
                let rx = new RegExp(key);

                if (rx.test(message)) {
                    err.header = err.header_replacements[key];
                }
            }
        }

        if (err.properties_replacements) {
            for (let key in err.properties_replacements) {
                let rx = new RegExp(key);

                if (rx.test(message)) {
                    for (let i = 0; i < err.properties_replacements[key].length; i++) {
                        let curKey = Object.keys(err.properties_replacements[key][i])[0];
                        err[curKey] = err.properties_replacements[key][i][curKey];
                    }

                }
            }
        }

        if (err.replacements) {
            for (let key in err.replacements) {
                let rx = new RegExp(key);

                if (rx.test(message)) {
                    err.message = err.replacements[key];
                }
            }
        }

        return err;
    }

    private handleError(exception) {
        const errorSettings = this.getErrorSettings(exception);
        if (!errorSettings) {
            return;
        }
        const code = exception.errorCode;
        if (!this.currentDisplayedErrors.hasOwnProperty(code)) {
            this.currentDisplayedErrors[code] = true;

        }

        let dialogRef = this.dialog.open(ErrorDialog, {
              width: '500px',
              data: {errorSettings}
            });

        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
        });

        }

}
