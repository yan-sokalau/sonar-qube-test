import * as errors from './errors.json';
import { Injectable } from '@angular/core';

Injectable()
export class Errors{
    public errors: Object = {};
    constructor() {
        Object.assign(this.errors, errors);
    }
}