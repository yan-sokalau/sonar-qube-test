export const GlobalEvents = {
    AUTH_SUCCESS: 'auth-events__auth-success',
    SESSION_EXPIRED: 'SessionExpired',
    APPLICATION_TICKETS_UPDATE: 'update-application-tickets'
};