export const USER_PRIVILEGES = {
    PRIVILEGE_ACCEPT_DOCUMENT: 'ACCEPT_DOCUMENT',
    PRIVILEGE_ADD_MANAGE_ANNOTATION: 'ADD_MANAGE_ANNOTATION',
    PRIVILEGE_ADMINISTRATE: 'ADMINISTRATE',
    PRIVILEGE_CHANGE_VISIBILITY_OF_ANNOTATION: 'CHANGE_VISIBILITY_OF_ANNOTATION',
    PRIVILEGE_EDIT_DOCUMENT_ATTRIBUTES: 'EDIT_DOCUMENT_ATTRIBUTES', // rename permissions
    PRIVILEGE_MARK_UNMARK_DECISION_DOCUMENT: 'MARK_UNMARK_DECISION_DOCUMENT',
    PRIVILEGE_REDACT_DOCUMENTS: 'REDACT_DOCUMENTS',
    PRIVILEGE_SORT_POST: 'SORT_POST',
    PRIVILEGE_VIEW_DOCUMENT: 'VIEW_DOCUMENT',
    PRIVILEGE_VIEW_INTERNAL_ANNOTATION: 'VIEW_INTERNAL_ANNOTATION',
    PRIVILEGE_VIEW_UNREDACTED_DOCUMENTS: 'VIEW_UNREDACTED_DOCUMENTS'
};

export interface IResponseUserDetail {
    id: string,
    firstName: string,
    lastName: string,
    roleName: string,
    emailId: string,
    userId: string
}

export class UserModel {
    public launchFlags: string;
    private id: string;
    private privilegeMap: Map<any, any> = new Map();
    private userDetail: IResponseUserDetail;
    private  responseData: Object;
    private expirySeconds: number;
    private senderPartyName: string;

    constructor(data) {
        const privileges = Array.isArray(data.privileges) ? data.privileges : [data.privileges];
        privileges.forEach((privilege) => {
            this.privilegeMap.set(privilege, true);
        });

        Object.assign(this, data);

        this.responseData =  data;

        this.id = data.id;
    }
    public getResponseData():Object{
        return this.responseData;
    }

    public getExpirationTime(): number{
        return this.expirySeconds * 1000;
    }

    public setResponseData(data):void{
        if (data.namedUserAuthenticationResult && data.namedUserAuthenticationResult.userTicket) {
            data = data.namedUserAuthenticationResult.userTicket;
        }
        this.responseData = data;
   }

    public getId() {
        return this.id;
    }

    public get firstName(): string {
        return this.userDetail.firstName;
    }

    public get lastName(): string {
        return this.userDetail.lastName;
    }

    public getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }

    public hasAdministrativePrivileges(): boolean {
        return this.privilegeMap.has(USER_PRIVILEGES.PRIVILEGE_ADMINISTRATE);
    }


    public hasPrivilege(privilegeName: String): boolean {
        let hasPrivilege: boolean = this.hasAdministrativePrivileges();

        if (!hasPrivilege) {
            hasPrivilege = this.privilegeMap.has(privilegeName);
        }

        return hasPrivilege;
    }

    public updateUserModelWithFlags(flags: string) {
        this.launchFlags = flags;
    }

}
