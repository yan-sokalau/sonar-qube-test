export class UrlsModel {
    private gatewayBaseURL: string;
    private uploadBaseURL: string;
    private downloadBaseURL: string;
    private docReaderURL: string;
    private submissionWidgetURL: string;
    private responseData: Object;


    // TODO no checking for missing values
    constructor(data) {

        this.gatewayBaseURL = data.filter((value) => {
            return value.key === 'gatewayBaseURL';
        })[0].value;

        this.uploadBaseURL = data.filter((value) => {
            return value.key === 'uploadBaseURL';
        })[0].value;

        this.downloadBaseURL = data.filter((value) => {
            return value.key === 'downloadBaseURL';
        })[0].value;

        this.docReaderURL = data.filter((value) => {
            return value.key === 'docReaderURL';
        })[0].value;

        this.submissionWidgetURL = data.filter((value) => {
            return value.key === 'submissionWidgetURL';
        })[0].value;

        if (this.docReaderURL[this.docReaderURL.length - 1] === '/') {
            this.docReaderURL = this.docReaderURL.slice(0, -1);
        }

        this.responseData = '';
    }

    public getResponseData(): Object {
        return this.responseData;
    }

    public setResponseData(data): void {
        this.responseData = data;
    }

    public getGatewayBaseURL(): string {
        return this.gatewayBaseURL;
    }

    public getUploadBaseURL(): string {
        return this.uploadBaseURL;
    }

    public getDownloadBaseURL(): string {
        return this.downloadBaseURL;
    }

    public getdocReaderURL(): string {
        return this.docReaderURL;
    }

    public getSubmissionWidgetURL(): string {
        return this.submissionWidgetURL;
    }
}
