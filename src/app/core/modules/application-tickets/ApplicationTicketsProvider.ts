import {ApplicationTicketModel} from './ApplicationTicketModel';
import {EventsService} from '../utils/event.service';
import {GlobalEvents} from '../utils/global.events';

export class ApplicationTicketsProvider {
    private applicationTickets = new Array<ApplicationTicketModel>();
    private ticketNum: number = 0;

    constructor(private eventsAggregator: EventsService) {
    }

    public initApplicationTickets(tickets: Array<any>): void {
        let _self = this; // TODO avoid _self
        _self.applicationTickets = [];

        tickets.forEach(function (ticketJsonObj) {
            let currentModel = new ApplicationTicketModel(ticketJsonObj);
            _self.applicationTickets.push(currentModel);
        });
    }

    public setApplicationTickets(tickets: Object[]): void {
        tickets.forEach((ticketJsonObj) => {
            let currentModel = new ApplicationTicketModel(ticketJsonObj);
            this.applicationTickets.push(currentModel);
        });
    }

    public getNumOfApplicationTickets(): number {
        return this.applicationTickets.length;
    }

    public getNumOfRelevantApplicationTickets(): number {
        this.removeExpiredTickets();
        return this.applicationTickets.length;
    }

    public getApplicationTicketById(id: string): ApplicationTicketModel {
        let ticket = this.applicationTickets.filter(function (ticket: ApplicationTicketModel) {
            return ticket.getId() == id;
        });
        if (ticket instanceof Array && ticket.length > 0) { //TODO wrap with utility array method
            return ticket[0];
        }
        return null;
    }

    public getNextApplicationTicketId(i: number = 0): Promise<any> {
        return new Promise((resolve, reject) => {
            let newTicketNum = this.ticketNum + 1;
            if (newTicketNum >= this.applicationTickets.length) {
                newTicketNum = this.ticketNum = 0;
            } else {
                this.ticketNum = newTicketNum;
            }
            let time = (new Date()).getTime();
            if ((time < this.applicationTickets[newTicketNum].getLastUsedTime() +
                this.applicationTickets[newTicketNum].expirySeconds * 1000) ||
                this.applicationTickets[newTicketNum].expirySeconds === 0) {
                this.applicationTickets[newTicketNum].lockApplicationTicket();
                this.applicationTickets[newTicketNum].setLastUsedTime();
                let id = this.applicationTickets[newTicketNum].getId();
                this.checkTicketsExperationTime().then(response => {
                    response ? resolve(id) : resolve(this.applicationTickets[0].getId())
                });
            } else {
                if (i < this.applicationTickets.length) {
                    i++;
                    reject(i);
                } else {
                    reject("error");
                }
            }
        }).catch(reject => {
            switch (reject) {
                case "error":
                    alert('INVALID_APPLICATION_TICKET')
                    //this.requests.exceptionService.error({errorCode: 'INVALID_APPLICATION_TICKET'});
                    break;
                default:
                    return this.getNextApplicationTicketId(reject);
            }
        });
    }

    public unlockApplicationTicket(id: string): void {
        let currTicket = this.getApplicationTicketById(id);
        if (currTicket) {
            currTicket.unlockApplicationTicket();
        }
    }

    public getApplicationTicketsForDirectLaunch(num: number): ApplicationTicketModel[] {
        num = this.applicationTickets.length - num;
        let toReturn = this.applicationTickets.slice(num);
        this.applicationTickets = this.applicationTickets.slice(0, num);
        return toReturn;
    }

    public getApplicationName(): string {
        return this.applicationTickets[0].getApplicationName();
    }

    public vaidateApplicationTickets(ticketId: string = ''): void {
        if (ticketId) {
            // this.requests.validateApplicationTicket(ticketId).then(
            //     (response) => {
            //         this.getApplicationTicketById(ticketId).setLastUsedTime();
            //     }
            // );
        }
    }

    private checkTicketsExperationTime(): Promise<any> {
        return new Promise((resolve) => {
            let time = (new Date()).getTime();
            let i = 0;
            for (let ticket of this.applicationTickets) {
                if (time < ticket.getLastUsedTime() + ticket.expirySeconds * 1000) {
                    i++;
                }
            }
            if (i < 4 && i > 0) {
                this.eventsAggregator.broadcast(GlobalEvents.APPLICATION_TICKETS_UPDATE, i);
            } else {
                resolve(true);
            }
        });
    }

    public getTicketsFromDirectLaunch(): Promise<any> {
        return new Promise((resolve) => {
            let parentWindow = window.parent;
            let listener = (event) => {
                resolve();
            };
            if (window.addEventListener) {
                window.addEventListener("message", listener);
            } else {
                // IE8
                window['attachEvent']("onmessage", listener);
            }
            parentWindow.postMessage('{"command" : "getApplicationTickets"}', "*");
        });
    }

    private removeExpiredTickets(): void {
        let relevantTickets: ApplicationTicketModel[] = [];
        let time: number = (new Date()).getTime();
        for (let ticket of this.applicationTickets) {
            if ((time < ticket.getLastUsedTime() + ticket.expirySeconds * 1000) || ticket.expirySeconds === 0) {
                relevantTickets.push(ticket);
            }
        }
        this.applicationTickets = relevantTickets;
    }

}
