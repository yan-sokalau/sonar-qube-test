export class ApplicationTicketModel {
    public expirySeconds: number;
    private id: string;
    private lastUsedTime: number;
    private isLocked: boolean;
    private applicationName: string;

    constructor(data) {
        Object.assign(this, data);
        this.isLocked = false;
        this.lastUsedTime = (new Date()).getTime();
    }

    public getId(): string {
        return this.id;
    }

    public setLastUsedTime(): void {
        this.lastUsedTime = (new Date()).getTime();
    }

    public getLastUsedTime(): number {
        return this.lastUsedTime;
    }

    public lockApplicationTicket(): void {
        this.isLocked = true;
    }

    public unlockApplicationTicket(): void {
        this.isLocked = false;
    }

    public getApplicationName(): string {
        return this.applicationName;
    }
}
