import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import {error} from "util";

interface Certificate {
    id: string;
}


@Injectable()
export class ConfigService {
    public name: string;
    public secret: string;
    public api: string;
    public api_prefix: string;
    public tickets: number;
    public version: string;
    public keys: Object;
    public production_keys: Object;
    public certificate: Certificate;
    public production_certificate: Certificate;
    public env: string;

    constructor(private http: Http) {
            this.getConfig();

    }


    private getConfig(){
        var request = new XMLHttpRequest();
        request.open('GET', './config/config.json', false);
        request.send(null);

        if (request.status === 200) {
          Object.assign(this, JSON.parse(request.responseText));
        }else{
          alert("Configuration not loaded");
        }
    }
}