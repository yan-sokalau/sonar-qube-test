import {UrlsModel, UserModel} from '../modules/models/index';
import {Requests} from '../modules/requests/Requests';
import {Crypto, SimpleCrypto} from '../modules/crypto/index';
import { Injectable } from '@angular/core';
import {EventsService} from '../modules/utils/event.service';
import {GlobalEvents} from '../modules/utils/global.events';


@Injectable()

export class AuthenticationService {
    private crypto: any = Crypto;
    private SimpleCrypto: any = SimpleCrypto;

    private static urls: UrlsModel;
    private static user: UserModel;


    public constructor(
                       private requests: Requests,
                       private eventAggregator: EventsService
                       ) {
        this.SimpleCrypto = new SimpleCrypto(this.requests.cfg);
        this.eventAggregator.on(GlobalEvents.APPLICATION_TICKETS_UPDATE, (numOfTicketsToUpdate) => {
            this.updateApplicationTicketsOnExpiration(numOfTicketsToUpdate);
        });
    }

    public setUrlsModel(urlsArray: Array<string>): void {
        try {
            AuthenticationService.urls = new UrlsModel(urlsArray);
        }catch (e) {
            console.error(e);
        }
    }

    public setUserModel(userObj: any): void {

        AuthenticationService.user = new UserModel(userObj);
    }

    public updateUserModelWithFlags(flags: string): void {
        AuthenticationService.user.updateUserModelWithFlags(flags);
    }


    public getUrls(): UrlsModel {
        return AuthenticationService.urls;
    }

    public getUser(): UserModel {
        return AuthenticationService.user;
    }

    public authenticate(principal: string, credential: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.requests.validateApplicationAndSite().then(
                response => {
                    let result = response['validateApplicationAndSiteResult'];
                    this.setUrlsModel(result.urls);
                    this.requests.setBaseUrl(this.getUrls().getGatewayBaseURL() + Requests.restPrefix);
                    this.getUrls().setResponseData(response);
                    this.requests.setSiteId(result.siteId);
                    let crypto = new this.crypto(result.responseId, credential, principal, this.requests.cfg);
                    let encryptedData = crypto.getCrypto();
                    this.requests.authenticate(result.siteId, principal, encryptedData).then(
                        (response) => {
                            const result = response['namedUserAuthenticationResult'];
                            this.setUserModel(result.userTicket);
                            this.getUser().setResponseData(response);
                            this.clearAndGenerateApplicationTicketsCollection(() => {
                                this.eventAggregator.broadcast(GlobalEvents.AUTH_SUCCESS);
                            });
                        },
                        (error) => {
                            reject(error);
                        }
                    );
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }

    public setValidateAppAndSiteResult(response): void {
        let result = response['validateApplicationAndSiteResult'];
        this.setUrlsModel(result.urls);
        let pageUrlConst = this.getUrls().getDownloadBaseURL() + '/ServePage?command=';
        this['settingService'].setPageUrl(pageUrlConst);
        this.requests.setBaseUrl(this.getUrls().getGatewayBaseURL() + Requests.restPrefix);
        this.requests.setSiteId(result.siteId);
    }

    public getSimpleCrypto(): any {
        return this.SimpleCrypto;
    }

    public launchWithOneTimeConfiguration(launchConfigurationId: string): Promise<any> {
        return new Promise((resolve) => {
            if (launchConfigurationId) {
                this.requests.validateApplicationAndSite().then(
                    (response) => {
                        let result = response['validateApplicationAndSiteResult'];
                        this.setUrlsModel(result.urls);
                        this.getUrls().setResponseData(response);
                        this.requests.setBaseUrl(this.getUrls().getGatewayBaseURL() + Requests.restPrefix);
                        this.requests.setSiteId(result.siteId);
                        var encryptedConfigurationId = this.SimpleCrypto.rsaEncrypt(launchConfigurationId);
                        this.requests.getOneTimeLaunchConfiguration(encryptedConfigurationId).then(
                            configuration => {
                                this.applyLaunchConfigurationUser(configuration);

                                const entry = configuration.launchConfiguration.applicationDataMap.entry;
                                let multipleInstancesAllowed: boolean = false;
                                for (const prop of (entry instanceof Array ? entry : [entry])) {
                                    if (prop.key === 'multipleInstancesAllowed') {
                                        multipleInstancesAllowed = prop.value;
                                    }
                                }

                                if (multipleInstancesAllowed === true) {
                                    this.generateApplicationTicketsCollection(() => {

                                    });
                                } else {
                                    this.clearAndGenerateApplicationTicketsCollection(() => {
                                    });
                                }

                                resolve(configuration);
                            }
                        );
                    },
                    error => {

                    }
                );
            }
        });
    }

    public applyLaunchConfigurationUser(configuration): void {
       this.setUserModel(configuration.launchConfiguration['userTicket']);
    }

    public analyseApplicationDataMap(entry){
        let applicationDataMap = {};
        if (Array.isArray(entry)) {
            entry.forEach(function (item) {
                applicationDataMap[item.key] = item.value;
            });
        } else if (typeof entry === 'object') {
            applicationDataMap[entry.key] = entry.value;
        }

        return applicationDataMap;
    }

    public generateApplicationTickets() {
        let numOfTicketsToGenerate =
            this.requests.numOfTickets -
            this.requests.getNextApplicationTicketProvider().getNumOfRelevantApplicationTickets();
        if (numOfTicketsToGenerate > 0) {
            this.requests.generateApplicationTicketsCollection(this.getUser().getId(), numOfTicketsToGenerate)
                .then((response) => {
                    let tickets = response.applicationTicketCollection.applicationTickets;
                    this.requests.getNextApplicationTicketProvider().setApplicationTickets(tickets);
                });
        }
    }

    private generateApplicationTicketsCollection(callback) {
        let userTicketId = this.getUser().getId();
        this.requests.generateApplicationTicketsCollection(userTicketId, this.requests.numOfTickets).then(
            response => {
                let applicationTickets = response['applicationTicketCollection'].applicationTickets;
                this.requests.setApplicationTickets(applicationTickets);
                callback();
            },
            error => {

            }
        )
    }

    private clearAndGenerateApplicationTicketsCollection(callback) {
        let userTicketId = this.getUser().getId();
        this.requests.clearAndGenerateApplicationTicketsCollection(userTicketId).then(
            response => {
                let applicationTickets = response['applicationTicketCollection'].applicationTickets;
                this.requests.setApplicationTickets(applicationTickets);
                callback();
            },
            error => {

            }
        )
    }

    public validateApplicationTicketsID(ticketId: string) {
        this.requests.getNextApplicationTicketProvider().vaidateApplicationTickets(ticketId);
    }

    public logout(logout: boolean): void {
        if (logout && AuthenticationService.user) {
            this.requests.logout().then(
                (response) => {

                },
                (error) => {

                }
            );
        }
    }

    static isAuthorized(): boolean {
        return typeof AuthenticationService.user != "undefined" && AuthenticationService.user.getId() != '';
    }

    private updateApplicationTicketsOnExpiration(i){
        if (!this.requests.directLaunch) {
            let numOfTicketsToGenerate = this.requests.numOfTickets - i;
            this.requests.generateApplicationTicketsCollection(AuthenticationService.user.getId(),
                numOfTicketsToGenerate)
                .then((response) => {
                        let applicationTickets = response['applicationTicketCollection'].applicationTickets;
                        Requests.applicationTicketsWorker.initApplicationTickets(applicationTickets);

                    },
                    error => {

                    });
        } else {
            Requests.applicationTicketsWorker.getTicketsFromDirectLaunch();
        }
    }


}
