import {
    ModuleWithProviders, NgModule,
    Optional, SkipSelf
} from '@angular/core';

import {AuthenticationService}   from './service/authentication.service';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import {ExceptionService}  from './modules/errors/exception.service';
import {Errors} from  './modules/errors/errors.config';
import {MaterialModule}   from '../material.module';
import {ErrorDialog} from './modules/errors/error-dialog/dialog';
import {ConfluenceService} from "../confluence/services/ConfluenceService";
import {XLSXService} from "../confluence/services/XLSXService";
import {Requests} from "./modules/requests/Requests";

@NgModule({
    imports: [RouterModule, MaterialModule],
    exports: [NavComponent],
    declarations: [NavComponent, ErrorDialog],
    providers: [ExceptionService, Errors, ConfluenceService, XLSXService, Requests],
    entryComponents: [ ErrorDialog]
})

export class CoreModule {
    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

    static forRoot(authenticationService: AuthenticationService): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: []
        };
    }
}
